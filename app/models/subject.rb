class Subject < ApplicationRecord
  has_many :grades, dependent: :destroy

end
