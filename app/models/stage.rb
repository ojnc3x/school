class Stage < ApplicationRecord
  extend FriendlyId
  friendly_id :title, use: [:slugged, :finders]
  
  has_many :school_classes, dependent: :destroy
end
