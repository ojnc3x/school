Rails.application.routes.draw do
  devise_for :users,
  controllers: {
    sessions: 'users/sessions',
    passwords: 'users/passwords'
  }

  mount Ckeditor::Engine => '/ckeditor'

  namespace :panel do
    root to: "dashboard#index"

    resources :events #actividades
    resources :grades #notas
    resources :school_classes #grados (1ero, ....)
    resources :subjects #materias
    resources :periods #periodos
    resources :stages #etapa escolar
    resources :contacts
    resources :profile, only: [:edit, :update]
    resources :users
  end
  root to: "panel/dashboard#index"

end
