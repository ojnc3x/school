class CreateSchoolClasses < ActiveRecord::Migration[5.0]
  def change
    create_table :school_classes do |t|
      t.references :stage
      t.string :title
      t.string :slug, index: true, uniq: true
      t.timestamps
    end
  end
end
