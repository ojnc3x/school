class CreateEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :events do |t|
      t.boolean :active, default: true
      t.string :title
      t.string :slug, index: true, uniq: true
      t.string :school_grades_ids
      t.text :place
      t.text :activity
      t.datetime :event_at
      t.timestamps
    end
  end
end
