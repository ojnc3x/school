class CreateStudentContacts < ActiveRecord::Migration[5.0]
  def change
    create_table :student_contacts do |t|
      t.references :user
      t.string :name
      t.string :phone
      t.string :email
      t.string :alt_phone
      t.text :address
      t.string :kin
      t.timestamps
    end
  end
end
