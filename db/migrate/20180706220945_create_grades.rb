class CreateGrades < ActiveRecord::Migration[5.0]
  def change
    create_table :grades do |t|
      t.string :note
      t.references :school_class
      t.references :subject
      t.references :period
      t.integer :month
      t.integer :year
      t.string :comment
      t.timestamps
    end
  end
end
