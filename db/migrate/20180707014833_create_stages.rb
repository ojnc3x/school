class CreateStages < ActiveRecord::Migration[5.0]
  def change
    create_table :stages do |t|
      t.string :title
      t.string :slug, index: true, uniq: true

      t.timestamps
    end
  end
end
