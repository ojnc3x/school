class CreateSubjects < ActiveRecord::Migration[5.0]
  def change
    create_table :subjects do |t|
      t.references :school_class
      t.string :title
      t.text :description
      t.timestamps
    end
  end
end
