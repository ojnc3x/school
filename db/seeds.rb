# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
if User.count == 0
  User.create!(name: 'Oscar', email: "ojnc.190888@gmail.com", lastname: "Nuñez", password: "payaso21", username: "dreadwolf")
  User.first.add_role("admin")
end
